package com.huang.blog.controller;


import com.huang.blog.entity.User;
import com.huang.blog.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 黄佳林
 * @since 2020-05-01
 */
@RestController
//@RequestMapping("/blog/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping("/test01")
    public String test01(){

        User user=new User();
        user.setId(1L);
        user.setName("ERROR");
        boolean b = userService.updateById(user);

        return user.toString();

    }

    @RequestMapping("/test02")
    public String test02(){

        //User user = userService.getById(1L);
        User user = userService.queryAll();

        return user.toString();

    }
}

