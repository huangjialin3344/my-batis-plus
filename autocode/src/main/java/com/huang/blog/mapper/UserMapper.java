package com.huang.blog.mapper;

import com.huang.blog.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 黄佳林
 * @since 2020-05-01
 */
//在对应的Mapper上面继承基本类BaseMapper
@Repository
public interface UserMapper extends BaseMapper<User> {

    User queryAll();
}
