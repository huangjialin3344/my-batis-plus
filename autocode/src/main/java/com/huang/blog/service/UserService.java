package com.huang.blog.service;

import com.huang.blog.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 黄佳林
 * @since 2020-05-01
 */
public interface UserService extends IService<User> {
    User queryAll();
}
