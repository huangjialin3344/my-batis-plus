package com.huang.blog.service.impl;

import com.huang.blog.entity.User;
import com.huang.blog.mapper.UserMapper;
import com.huang.blog.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 黄佳林
 * @since 2020-05-01
 */
@Service
@Repository
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryAll() {
        return userMapper.queryAll();
    }
}
