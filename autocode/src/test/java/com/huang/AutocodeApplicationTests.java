package com.huang;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huang.blog.entity.User;
import com.huang.blog.mapper.UserMapper;
import com.huang.blog.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@SpringBootTest
class AutocodeApplicationTests {

    @Autowired
    UserMapper userMapper;
    @Autowired
    UserServiceImpl userService;

    @Test
    void contextLoads() {
    }
    //集合查询
    @Test
    public void testSelectById(){
        //Collection<User> users = userService.listByIds(Arrays.asList(1L, 2L, 3L));
        List<User> userList = userMapper.selectBatchIds(Arrays.asList(1L, 2L, 3L));
        System.out.println(userList);
    }

    @Test
    void test() {
        // 查询name不为空的用户，并且邮箱不为空的用户，年龄大于等于12
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper
                .isNotNull("name")
                .isNotNull("email")
                .ge("age",12);
        userService.list(wrapper).forEach(System.out::println);
        //userMapper.selectList(wrapper).forEach(System.out::println); // 和我们刚才学习的map对比一下
    }
    @Test
    void test2() {
        User users = userService.queryAll();
        System.out.println(users);
    }

}
