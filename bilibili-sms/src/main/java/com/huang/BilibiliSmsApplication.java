package com.huang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class BilibiliSmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BilibiliSmsApplication.class, args);
    }

}
