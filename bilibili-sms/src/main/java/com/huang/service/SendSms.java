package com.huang.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface SendSms {
    public boolean send(String phoneNum, String templateCode, Map<String, Object> code);
}
