package com.huang;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.HashMap;

@SpringBootTest
class BilibiliSmsApplicationTests {

    @Test
    void contextLoads() {

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GFVCrruY5nhECH7EfV5", "9SGJb9bLJ7G6LuuDnZmhPmvLLyssMk");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");

        //自定义参数（手机号，验证码，签名，模板）
        request.putQueryParameter("PhoneNumbers", "17638164389");
        request.putQueryParameter("SignName", "跟着狂神学Java");//待完善
        request.putQueryParameter("TemplateCode", "SMS_189521253");
        //构建一个短信验证码
        HashMap<String,Object> map=new HashMap<>();
        map.put("code","1314");
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));//JSONObject.toJSONString(map)

        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            System.out.println("发送成功？！");
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }

    }

}
