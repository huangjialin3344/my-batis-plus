package com.huang.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @TableId(type=IdType.INPUT)//自增，数据库也必须设置自增
    private Long id;
    private String name;
    private Integer age;
    private String email;
    @Version//乐观锁
    private Integer version;

    @TableLogic//逻辑删除
    private Integer deleted;

    //自动填充
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date update_time;
}
