package com.huang;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huang.mapper.UserMapper;
import com.huang.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MybatisPlusApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        //查询全部用户
        //参数是一个Wrapper，条件构造器
        List<User> users=userMapper.selectList(null);
        System.out.println(users);
        users.forEach(System.out::println);
    }

    //测试插入
    @Test
    public void testInsert(){
        User user=new User();
        user.setId(7L);
        user.setName("lin");
        user.setAge(22);
        user.setEmail("2622046365@qq.com");
        //.var
        int insert = userMapper.insert(user);//帮我们自动生成id 雪花算法
        System.out.println(insert);
    }

    //测试更新
    @Test
    public void testUpdate(){
        User user=new User();
        user.setId(7L);
        user.setName("change");

        userMapper.updateById(user);
    }

    //测试乐观锁成功
    @Test
    public void testLock(){
        //查询用户信息
        User user=userMapper.selectById(1L);
        //修改用户信息
        user.setName("change02");
        //执行操作
        userMapper.updateById(user);
    }
    //测试乐观锁失败
    @Test
    public void testLock2(){
        //线程1
        User user=userMapper.selectById(1L);
        user.setName("change02");
        //线程2
        User user2=userMapper.selectById(1L);
        user2.setName("change03");
        userMapper.updateById(user2);

        //自旋锁去操作
        userMapper.updateById(user);//如果没有乐观锁就会覆盖插队线程
    }

    //集合查询
    @Test
    public void testSelectById(){
        List<User> userList = userMapper.selectBatchIds(Arrays.asList(1L, 2L, 3L));
        System.out.println(userList);
    }

    //条件查询map
    @Test
    public void testSelectByIds(){
        HashMap<String,Object> map=new HashMap<>();
        //自定义查询
        map.put("name","change");

        List<User> userList = userMapper.selectByMap(map);
        System.out.println(userList);
    }

    //分页查询
    @Test
    public void testPage(){
        //当前页
        //页面大小
        Page<User> page = new Page<>(1, 5);
        userMapper.selectPage(page, null);

        page.getRecords().forEach(System.out::println);
        //page.getTotal()
        //System.out.println(userIPage);
    }

    //测试删除
    @Test
    public void testDeleteById(){
        userMapper.deleteById(7L);//方法很多，和select对照
    }
}
